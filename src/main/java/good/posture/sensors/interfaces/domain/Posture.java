package good.posture.sensors.interfaces.domain;

import java.util.UUID;

public class Posture {

	private UUID id;
	
	private Integer value;
	
	public Posture(Integer value) {
		this.id = UUID.randomUUID();
		this.value = value;
	}

	public UUID getId() {
		return id;
	}

	public Integer getValue() {
		return value;
	}
	
}
