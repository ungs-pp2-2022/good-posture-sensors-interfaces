package good.posture.sensors.interfaces;

import java.util.List;

import good.posture.sensors.interfaces.domain.Posture;

public interface PostureHistorial {

	boolean save(Posture posture);
	
	List<Posture> findAll();
}
